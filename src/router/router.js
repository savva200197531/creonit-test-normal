import Vue from "vue";
import Router from 'vue-router';
import vMainWrapper from "../components/v-main-wrapper";
import vGoods from "../components/goods/v-goods";
import vCart from "../components/cart/v-cart";

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'v-main-wrapper',
      component: vMainWrapper,
    },
    {
      path: '/goods',
      name: 'v-goods',
      component: vGoods,
    },
    {
      path: '/cart',
      name: 'v-cart',
      component: vCart,
      props: true
    }
  ]
})

export default router;
